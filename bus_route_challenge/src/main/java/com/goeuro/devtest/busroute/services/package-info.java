/**
 * This package holds all the classes responsible for reading, parsing and otherwise handling bus route data from
 * different sources.
 * 
 * @author aelsayed
 *
 */
package com.goeuro.devtest.busroute.services;
