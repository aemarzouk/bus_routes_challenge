package com.goeuro.devtest.busroute;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Application's main class.
 * 
 * @author aelsayed
 *
 */
// CHECKSTYLE:OFF: checkstyle:HideUtilityClassConstructor -- Spring needs this class not to be a utility class
@SuppressWarnings("PMD.UseUtilityClass")
@SpringBootApplication
public class BusRouteChallengeApplication {
    private static final String className = BusRouteChallengeApplication.class.getName();
    private static final Logger logger = Logger.getLogger(className);

    /**
     * The application entry point.<br>
     * Used to load and run the Spring Application Context.
     * 
     * @param args
     *            Command line arguments.
     */
    public static void main(final String[] args) {
        // Validate that the command line arguments include the mandatory file path option
        logger.entering(className, "main");
        if (!validateArgs(args)) {
            return; // If the arguments are not valid, exit without initializing the Spring Application.
        }

        SpringApplication.run(BusRouteChallengeApplication.class, args);
    }

    /**
     * Validates that the --filePath argument has been passed.<br>
     * <b>Note:</b> No verificatoin is performed here.
     * 
     * @param args
     *            The command line arguments.
     * @return True if the --filePath has been passed in the command line arguments.
     */
    @SuppressWarnings("PMD.SystemPrintln")
    protected static boolean validateArgs(final String[] args) {
        boolean isFilePathGiven = false;

        if (args != null) { // This should never happen if the args array is received from the command line args.
                            // However, it is always good to keep the code robust.
            for (String a : args) {
                if (a.trim().startsWith("--filePath=")) {
                    isFilePathGiven = true;
                }
            }
        }

        if (!isFilePathGiven) {
            System.out.println("No weekly routes file path given.");
            System.out.println("Please re-run the application with the command line argument --filePath=<FILE_PATH>");
            logger.finer("Application was run without filePath parameter."); // To be kept in log file if needed
        }

        return isFilePathGiven;
    }

}
