# Bus Route Challenge

> Developed by **Ahmed E. Marzouk** for **GoEuro**

## About

This is a task developed to demonstrate a simple Spring Microservice application. The application parses a simple text file which describes the different bus routes available then uses this data to determine whether a direct route exists between any 2 stations.

> Note: The application *by default* assumes that all bus routes are bi-directional. This means that a "direct path" is exists for all stations in the same route regardless of their relative positions. **This default behavior can be overridden.**
>
> Please check the assumptions section at the end of this file.

### Compilation

The code is packaged as a Maven project. It can be built using standard Maven targets.

The default goal (and the recommended one) is:

```shell
mvn clean package
```

This goal performs the following tasks:

1. Clean the project.
2. Build the project.
3. Run all JUnit test cases (using Sure-Fire) and calculate code coverage (using Jacoco).
4. Packages the compiled application into a single executable JAR (under the `/target` folder).

## Usage

### Using service.sh

The application can be built and ran using the supplied *service.sh* file.

### Manual execution

To run the packaged application, run the following command from the command prompt:

```shell
java -jar target/bus_route_challenge-1.0.jar --filePath=[FILE PATH]
```

> **FILE PATH:** The input file path. *The application will not run without this parameter as the routes data is fully parsed and indexed during initialization.*
>

## Docker

Dockerfile is provided so the application can be compiled as a Docker image.

**Note:** The input file path must be passed to the *docker build* command so that the file is baked into the image at build time. This allows the resultant Docker image to run directly without having to mount the file manually at run time.

You can build the Docker image using the *build_docker <file_path>* target in *service.sh*.

Or you can build it manually using the following command:

```shell
docker build . -t goeuro:devtest --build-arg inputFile=[FILE PATH]
```

> **FILE PATH:** The input file path. *The application will not run without this parameter as the routes data is fully parsed and indexed during initialization.*

## Swagger

The REST API is documented using Swagger 2.0 and visualized using Swagger UI.

The Swagger file can be accessed at runtime through the URL: http://localhost:8088/api/v2/api-docs

Swagger UI can be accessed at runtime through the URL: http://localhost:8088/api/swagger-ui.html

## Code Analysis and Reports

You can generate code analysis reports using `./service.sh dev_site` or `mvn clean site`

The following reports are automatically generated and added to the HTML website under `target/site/`:

* Java Docs
* Junit unit testing (Surefire Reports)
* JaCoCo code coverage
* Project dependencies
* Project information

## Assumptions

1. The requirements did not clarify whether the bus routes are **bidirectional** or **unidirectional**. If they are unidirectional, a direct path exists only if the *departure station* is before the *arrival station* in the same route. If the routes are bidirectional, a direct path exists between all stations on the same route.
   1. The code and data structure are designed to support both cases (Hence, the need for the *StationInRoute* inner class which keeps track of each station's position in a route).
   2. Spring configuration parameter `isBidirectional` is used to control the application's behavior.
   3. Default value for isBidirectional is **true**. This default was chosen because it matches the actual bus routes in *Berline*, *Cairo* and *Johannesburg*.
   4. You can override this behavior by changing the *isBidirectional* proprty in `application.properties`
   5. You can also use the command line argument `--isBidirectional=false` to set the application to unidirectional mode when running it from the command line.

