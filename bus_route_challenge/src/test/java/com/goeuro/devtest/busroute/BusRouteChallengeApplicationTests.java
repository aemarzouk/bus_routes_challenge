package com.goeuro.devtest.busroute;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.goeuro.devtest.busroute.api.WeeklyBusRouteRestController;
import com.goeuro.devtest.busroute.services.WeeklyBusRouteService;

/**
 * Smoke test.<br>
 * Used to verify that Spring is loaded correctly and is able to initialize all the main beans.
 * 
 * @author aelsayed
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BusRouteChallengeApplicationTests {

    @Autowired
    private WeeklyBusRouteRestController weeklyBusRouteRestController;

    @Autowired
    private WeeklyBusRouteService weeklyBusRouteService;

    /**
     * Make sure Spring Context is loaded correctly and that it was able to inject all beans.
     */
    @Test
    public void contextLoads() {
        assertThat(weeklyBusRouteRestController).isNotNull();
        assertThat(weeklyBusRouteService).isNotNull();
    }

    /**
     * {@link BusRouteChallengeApplication#validateArgs(String[])} Should return true if --filePath is passed.
     */
    @Test
    public void validateArgsShouldReturnTrueIfFilePathIsGiven() {
        final String[] validArgs = new String[] { "--filePath=/example" };
        assertTrue(BusRouteChallengeApplication.validateArgs(validArgs));
    }

    /**
     * {@link BusRouteChallengeApplication#validateArgs(String[])} Should return false if --filePath is not passed.
     */
    @Test
    public void validateArgsShouldReturnFalseIfFilePathIsNotGiven() {
        assertFalse(BusRouteChallengeApplication.validateArgs(null));
        assertFalse(BusRouteChallengeApplication.validateArgs(new String[] {}));
        assertFalse(BusRouteChallengeApplication.validateArgs(new String[] { "" }));
        assertFalse(BusRouteChallengeApplication.validateArgs(new String[] { "--filepath=/example" })); // Notice the
                                                                                                        // lowercase p
    }
}
