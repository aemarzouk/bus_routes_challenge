package com.goeuro.devtest.busroute.services;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.goeuro.devtest.busroute.exceptions.InvalidWeeklyBusRoutesFileException;

/**
 * Responsible for parsing and indexing the Weekly Bus Route Data files.
 * 
 * @author aelsayed
 *
 */
@Service
public class WeeklyBusRouteService {

    private static final String className = WeeklyBusRouteService.class.getName();
    private static final Logger logger = Logger.getLogger(className);
    private static final int MIN_NUMBER_OF_STATIONS_PER_ROUTE = 1; // Perhaps this should be 2? Since we have no solid
                                                                   // requirements for this validation, we will use 1.
    private final boolean isBusRoutesBidirectional;

    private final BufferedReader inpuFileReader;

    // The key is the station id. For each station, a PriorityQueue stores the list of routes it belongs to and its
    // position in each route.
    private final Map<Integer, TreeSet<StationInRoute>> stationsRoutesMap =
            new HashMap<Integer, TreeSet<StationInRoute>>();

    /**
     * Constructor.<br>
     * Reads and indexes the file with the given <code>filePath</code>.
     * 
     * @param filePath
     *            The absolute file path (including file name) for the file to be parsed.
     * @param isBusRoutesBidirectional
     *            Indicates whether the bus routes are bidirectional.<br>
     *            <i><u>true:</u></i> Bus routes are bidirectional.<br>
     *            Direct route is found if the departure and arrival stations are on the same route regardless of
     *            order.<br>
     *            <i><u>false:</u></i> Bus routes are unidirectional.<br>
     *            Direct route is found only if the departure and arrival stations are on the same route <i>AND</i>
     *            departure station is before arrival station.
     * @throws FileNotFoundException
     *             If the file was not found for any reason.
     * @throws InvalidWeeklyBusRoutesFileException
     *             If the given file does not conform to the Weekly Bus Route file specifications.
     */
    public WeeklyBusRouteService(@Value("${filePath}") final String filePath,
            @Value("${isBidirectional}") final boolean isBusRoutesBidirectional)
            throws FileNotFoundException, InvalidWeeklyBusRoutesFileException {
        final String methodName = className + "(String)";
        logger.entering(className, methodName, filePath);
        logger.info("Reading bus routes data from: " + filePath);
        logger.info(isBusRoutesBidirectional ? "Bus routes are BIDIRECTIONAL" : "Bus routes are UNIDIRECTIONAL");
        this.isBusRoutesBidirectional = isBusRoutesBidirectional;

        logger.fine("Trying to open file: " + filePath);
        // FileReader reads text files in the default encoding.
        final FileReader fileReader = new FileReader(filePath);

        // Always wrap FileReader in BufferedReader.
        inpuFileReader = new BufferedReader(fileReader);
        logger.fine("Successfully opened file: " + filePath);

        // Read the number of routes
        final int numberOfRoutes;
        try {
            logger.fine("Parsing number of routes");
            numberOfRoutes = Integer.parseInt(inpuFileReader.readLine());
            logger.info("Number of routes = " + numberOfRoutes);
        } catch (NumberFormatException | IOException e) {
            final InvalidWeeklyBusRoutesFileException ex = new InvalidWeeklyBusRoutesFileException(e);
            logger.throwing(className, methodName, ex);
            throw ex;
        }

        for (int i = 1; i <= numberOfRoutes; ++i) {
            logger.fine("START parsing route at line number " + (i + 1)); // Adding 1 to account for the 1st line in the
                                                                          // file which was read before this loop.
            parseAndIndexRouteInfoLine();
            logger.fine("END parsing route at line number " + (i + 1));
        }
        logger.info("Number of unique stations indexed = " + stationsRoutesMap.size());
    }

    /**
     * Reads a single line from the Weekly Bus Routes file and parses it.<br>
     * All the parsed data is added to the <code>stationsRoutesMap</code> object.<br>
     * <br>
     * The line is expected to consist of a list of space-separated integers whereas the first integer represents the
     * route number and the remaining integers represent station ids for the stations in that route.<br>
     * 
     * @throws InvalidWeeklyBusRoutesFileException
     *             If the line read does not match the expected format.
     */
    private void parseAndIndexRouteInfoLine() throws InvalidWeeklyBusRoutesFileException {
        try {
            logger.entering(className, "parseAndIndexRouteInfoLine");
            final String routeInfoLine = inpuFileReader.readLine();
            final String[] routeInfoSplitString = routeInfoLine.trim().split(" "); // Trimming, just in case.

            // Validation: The route must have the minimum number of stations at least.
            if (routeInfoSplitString.length < 1 + MIN_NUMBER_OF_STATIONS_PER_ROUTE) { // Adding 1 for the route id
                final InvalidWeeklyBusRoutesFileException ex = new InvalidWeeklyBusRoutesFileException();
                logger.throwing(className, className + "(String)", ex);
                throw ex;
            }

            // Read the route id
            final Integer routeId = Integer.parseInt(routeInfoSplitString[0]);
            logger.fine("Route ID = " + routeId);

            // Iterate over all the stations in the route
            for (int i = 1; i < routeInfoSplitString.length; ++i) { // Start at 1 to skip the route ID
                final Integer stationId = Integer.parseInt(routeInfoSplitString[i]);
                final StationInRoute stationInRoute = new StationInRoute(routeId, i); // The stations' positions are
                                                                                      // 1-indexed since the 0 index is
                                                                                      // used by the route id. No need
                                                                                      // to add or subtract anything :)
                if (stationsRoutesMap.containsKey(stationId)) {
                    // Station exists in the map. We need only to update its PriorityQueue.
                    final TreeSet<StationInRoute> p = stationsRoutesMap.get(stationId);
                    p.add(stationInRoute);
                } else {
                    final TreeSet<StationInRoute> p = new TreeSet<>();
                    p.add(stationInRoute);
                    stationsRoutesMap.put(stationId, p);
                }
            }
        } catch (IOException e) {
            final InvalidWeeklyBusRoutesFileException ex = new InvalidWeeklyBusRoutesFileException(e);
            logger.throwing(className, className + "(String)", ex);
            throw ex;
        }

        logger.exiting(className, "parseAndIndexRouteInfoLine");
    }

    /**
     * Checks whether there is a direct route between <code>departureStation</code> and <code>arrivalStation</code>.<br>
     * <b>Note:</b> The calculation varies depending on the value of the <code>isBusRoutesBidirectional</code> flag
     * which is set in the constructor. If the routes are bidirectional, this method only checks that both stations
     * exist on the same route. Otherwise, this method must check also that the departure station precedes the arrival
     * station on the route.
     * 
     * @param departureStation
     *            The departure station id (station number).
     * @param arrivalStation
     *            The arrival station id (station number).
     * @return true if there is a direct path between the two stations. (Please check the note in the method description
     *         for details on the definition of "direct route").
     */
    public boolean hasDirectPath(final Integer departureStation, final Integer arrivalStation) {
        final String methodName = "hasDirectPath";
        logger.entering(className, methodName, departureStation + ", " + arrivalStation);
        if (stationsRoutesMap.get(departureStation) == null || stationsRoutesMap.get(arrivalStation) == null) {
            logger.info("Departure or Arrival station does not exist. Returning false.");
            logger.exiting(className, methodName, false);
            return false; // One or both stations don't exist.
        }

        final StationInRoute[] departureRoutes = stationsRoutesMap.get(departureStation).toArray(new StationInRoute[0]);
        final StationInRoute[] arrivalRoutes = stationsRoutesMap.get(arrivalStation).toArray(new StationInRoute[0]);
        if (departureRoutes.length == 0 || arrivalRoutes.length == 0) { // This shouldn't happen, but we are being safe.
            logger.severe("departureRoutes.length == 0 || arrivalRoutes.length == 0"
                    + " --> This shouldn't happen due to the null checks earlier." + System.lineSeparator()
                    + "Method is returning false without throwing exception."
                    + " Please review the code to make sure this is the correct behavior.");
            logger.exiting(className, methodName, false);
            return false; // One of the two stations does not have any routes in the map.
        }

        // Trivial case: departure and arrival stations are the same station
        if (departureStation.equals(arrivalStation)) {
            logger.info("Departure and arrival stations are the same. Returning true.");
            logger.exiting(className, methodName, true);
            return true;
        }

        int departureIndex = 0;
        int arrivalIndex = 0;

        // The following loop is a modified implementation of "2 sorted arrays intersection" algorithm.
        // The loop keeps a pointer on each of the arrays and advances the one with the lower route value in each step.
        // If the routes match, we check for the bidirectional conditions and return true or advance both pointers by 1
        while (departureIndex < departureRoutes.length && arrivalIndex < arrivalRoutes.length) {
            if (departureRoutes[departureIndex].getRoute() == arrivalRoutes[arrivalIndex].getRoute()) {
                if (isBusRoutesBidirectional) {
                    logger.info("Found bidirectional path in route " + departureRoutes[departureIndex].getRoute());
                    logger.exiting(className, methodName, true);
                    return true;
                } else {
                    if (departureRoutes[departureIndex].getPosition() <= arrivalRoutes[arrivalIndex].getPosition()) {
                        logger.info("Found unidirectional path in route " + departureRoutes[departureIndex].getRoute());
                        logger.exiting(className, methodName, true);
                        return true;
                    } else {
                        ++departureIndex;
                        ++arrivalIndex;
                    }
                }

            } else {
                if (departureRoutes[departureIndex].getRoute() < arrivalRoutes[arrivalIndex].getRoute()) {
                    ++departureIndex;
                } else {
                    ++arrivalIndex;
                }
            }
        }

        logger.info("No direct routes found.");
        logger.exiting(className, methodName, false);
        return false;
    }

    /**
     * Represents a station's position in a route.<br>
     * Stores the route number of one of the routes that pass through the station in addition to the position (number)
     * of the station in that route.<br>
     * 
     * @author aelsayed
     *
     */
    static class StationInRoute implements Comparable<StationInRoute> {
        private final int route;
        private final int position;

        /**
         * Constructor.
         * 
         * @param route
         *            The route number of one of the routes that pass through the station.
         * @param position
         *            The position (number) of the station in the route.
         */
        StationInRoute(final int route, final int position) {
            this.route = route;
            this.position = position;
        }

        /**
         * Getter.
         * 
         * @return The route number of one of the routes that pass through the station.
         */
        public int getRoute() {
            return route;
        }

        /**
         * Getter.
         * 
         * @return The position (number) of the station in the route.
         */
        public int getPosition() {
            return position;
        }

        /**
         * Compares two instances of the class so that they can be sorted.<br>
         * This class should be sorted by route number. If the route numbers are equal, this method compares the
         * positions.
         * 
         * @param other
         *            The other instance of the class.
         */
        @Override
        public int compareTo(final StationInRoute other) {
            if (this.route == other.route) {
                return this.position - other.position;
            }
            // else
            return this.route - other.route;
        }

        // Overriding hashCode and equals to facilitate testing
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + position;
            result = prime * result + route;
            return result;
        }

        // Overriding hashCode and equals to facilitate testing
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof StationInRoute)) {
                return false;
            }
            final StationInRoute other = (StationInRoute) obj;
            if (position != other.position) {
                return false;
            }
            if (route != other.route) {
                return false;
            }
            return true;
        }

    }
}
