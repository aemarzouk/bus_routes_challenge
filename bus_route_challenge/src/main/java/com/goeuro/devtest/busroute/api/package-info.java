/**
 * This package holds all the public API end-points.
 * 
 * @author aelsayed
 *
 */
package com.goeuro.devtest.busroute.api;
