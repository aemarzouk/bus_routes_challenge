package com.goeuro.devtest.busroute.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.goeuro.devtest.busroute.services.WeeklyBusRouteService.StationInRoute;

/**
 * Test class for {@link WeeklyBusRouteService}.
 * 
 * @author aelsayed
 * 
 */
@SuppressWarnings("PMD.SignatureDeclareThrowsException") // This is a test class. It is OK to let the test case fail
// by throwing any exception.
@RunWith(SpringRunner.class)
@SpringBootTest
public class WeeklyBusRouteServiceTest {

    private static final String EXAMPLE_FILE_PATH = "src/test/resources/example";
    private static final String STATIONS_ROUTES_MAP_FIELD_NAME = "stationsRoutesMap";

    @Autowired
    private WeeklyBusRouteService springLoadedBusRouteService;

    /**
     * Validate that the constructor finishes properly when the given <code>filePath</code> is valid.
     * 
     * @see WeeklyBusRouteService#WeeklyBusRouteService(String, boolean)
     * 
     * @throws Exception
     *             if anything goes wrong
     */
    @Test
    public void constructorShouldNotThrowExceptionsForValidFilePath() throws Exception {
        new WeeklyBusRouteService("src/test/resources/example", true);
    }

    /**
     * Verify that the constructor throws the expected exception if the given <code>filePath</code> does not exist.
     * 
     *             if anything goes wrong
     */
    @Test
    public void constructorShouldThrowExceptionsForNonExistentFilePath() {

        // CHECKSTYLE:OFF: checkstyle:illegalcatch -- This is a test case. It is OK to catch general Exceptions
        try {
            new WeeklyBusRouteService("src/test/resources/non-existent-file", true);
            fail("No exception was thrown.");
        } catch (final FileNotFoundException e) {
            // Do nothing. This is the desired behavior.
        } catch (final Exception e) {
            fail("Wrong exception type was thrown.");
        }
        // CHECKSTYLE:ON: checkstyle:illegalcatch
    }

    /**
     * Verify that the routes info was indexed correctly after constructor completes.<br>
     * 
     * @see WeeklyBusRouteService#WeeklyBusRouteService(String, boolean)
     * 
     * @throws Exception
     *             if anything goes wrong
     */
    @Test
    public void routesInfoShouldBeIndexedAfterConstructor() throws Exception {
        // The stationsRoutesMap object is (and should be) private. We have to access it by reflection for this test
        // case.
        final Field stationRoutesMapField =
                springLoadedBusRouteService.getClass().getDeclaredField(STATIONS_ROUTES_MAP_FIELD_NAME);
        stationRoutesMapField.setAccessible(true);
        @SuppressWarnings("unchecked")
        final Map<Integer, TreeSet<StationInRoute>> stationsRoutesMap =
                (Map<Integer, TreeSet<StationInRoute>>) stationRoutesMapField.get(springLoadedBusRouteService);

        // Start testing

        // Make sure that the number of stations parsed equal the number of stations in the source file
        // CHECKSTYLE:OFF: checkstyle:magicnumber
        if (stationsRoutesMap.size() != 24) {
            fail("Incorrect number of stations parsed.");
        }

        // Validate a sample of the stations
        final StationInRoute[] stationMaxIntExpected = new StationInRoute[] { new StationInRoute(1, 1),
                new StationInRoute(13, 1), new StationInRoute(18, 1), new StationInRoute(19, 1) };
        final StationInRoute[] station155Expected = new StationInRoute[] { new StationInRoute(11, 7) };
        final StationInRoute[] station5Expected =
                new StationInRoute[] { new StationInRoute(2, 1), new StationInRoute(6, 1), new StationInRoute(7, 4),
                        new StationInRoute(18, 3), new StationInRoute(19, 5) };
        final StationInRoute[] stationNegative121Expected =
                new StationInRoute[] { new StationInRoute(7, 1), new StationInRoute(11, 1), new StationInRoute(19, 2) };

        // Note: station 153 in the input example was renamed to Integer.MAX_INT and station 121 has been renamed to
        // "-121" in order to be used in edge case testing
        final StationInRoute[] stationMaxIntActual = stationsRoutesMap.get(2147483647).toArray(new StationInRoute[0]);
        final StationInRoute[] station155Actual = stationsRoutesMap.get(155).toArray(new StationInRoute[0]);
        final StationInRoute[] station5Actual = stationsRoutesMap.get(5).toArray(new StationInRoute[0]);
        final StationInRoute[] stationNegative121Actual = stationsRoutesMap.get(-121).toArray(new StationInRoute[0]);

        if (!Arrays.equals(stationMaxIntExpected, stationMaxIntActual)) {
            fail("Station {MAX INT} was not parsed correctly");
        }
        if (!Arrays.equals(station155Expected, station155Actual)) {
            fail("Station 155 was not parsed correctly");
        }
        if (!Arrays.equals(station5Expected, station5Actual)) {
            fail("Station 5 was not parsed correctly");
        }
        if (!Arrays.equals(stationNegative121Expected, stationNegative121Actual)) {
            fail("Station -121 was not parsed correctly");
        }
    }

    /**
     * {@link WeeklyBusRouteService#hasDirectPath(Integer, Integer)} should return true if both stations are on the same
     * route regardless of their order when the {@link WeeklyBusRouteService} is initialized to be bidirectional.
     * 
     * @see WeeklyBusRouteService#hasDirectPath(Integer, Integer)
     * 
     * @throws Exception
     *             If anything goes wrong.
     */
    @Test
    public void hasDirectPathShouldReturnTrueWhenDirectPathExists_BIDIRECTIONAL() throws Exception {

        // Departure station < Arrival station
        assertTrue(springLoadedBusRouteService.hasDirectPath(2147483647, 24));
        assertTrue(springLoadedBusRouteService.hasDirectPath(140, 24));
        assertTrue(springLoadedBusRouteService.hasDirectPath(16, 155));
        assertTrue(springLoadedBusRouteService.hasDirectPath(-121, 155));
        assertTrue(springLoadedBusRouteService.hasDirectPath(5, 184));

        // Departure station > Arrival station
        assertTrue(springLoadedBusRouteService.hasDirectPath(24, 2147483647));
        assertTrue(springLoadedBusRouteService.hasDirectPath(24, 140));
        assertTrue(springLoadedBusRouteService.hasDirectPath(155, 16));
        assertTrue(springLoadedBusRouteService.hasDirectPath(155, -121));
        assertTrue(springLoadedBusRouteService.hasDirectPath(184, 5));

        // Departure station == Arrival station
        assertTrue(springLoadedBusRouteService.hasDirectPath(2147483647, 2147483647));
        assertTrue(springLoadedBusRouteService.hasDirectPath(140, 140));
        assertTrue(springLoadedBusRouteService.hasDirectPath(16, 16));
        assertTrue(springLoadedBusRouteService.hasDirectPath(-121, -121));
        assertTrue(springLoadedBusRouteService.hasDirectPath(5, 5));
    }

    /**
     * {@link WeeklyBusRouteService#hasDirectPath(Integer, Integer)} should return true ONLY if both stations are on the
     * same route AND Departure station is before Arrival station WHEN the {@link WeeklyBusRouteService} is initialized
     * to be bidirectional.
     * 
     * @see WeeklyBusRouteService#hasDirectPath(Integer, Integer)
     * 
     * @throws Exception
     *             If anything goes wrong.
     */
    @Test
    public void hasDirectPathShouldReturnTrueWhenDirectPathExists_UNIDIRECTIONAL() throws Exception {
        final WeeklyBusRouteService unidirectionalBusRouteService = new WeeklyBusRouteService(EXAMPLE_FILE_PATH, false);
        // Departure station < Arrival station
        assertTrue(unidirectionalBusRouteService.hasDirectPath(2147483647, 24));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(140, 24));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(16, 155));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(-121, 155));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(5, 184));

        // Departure station > Arrival station
        assertFalse(unidirectionalBusRouteService.hasDirectPath(24, 2147483647));
        assertFalse(unidirectionalBusRouteService.hasDirectPath(24, 140));
        assertFalse(unidirectionalBusRouteService.hasDirectPath(155, 16));
        assertFalse(unidirectionalBusRouteService.hasDirectPath(155, -121));
        assertFalse(unidirectionalBusRouteService.hasDirectPath(184, 5));

        // Departure station == Arrival station
        assertTrue(unidirectionalBusRouteService.hasDirectPath(2147483647, 2147483647));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(140, 140));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(16, 16));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(-121, -121));
        assertTrue(unidirectionalBusRouteService.hasDirectPath(5, 5));
    }

    /**
     * {@link WeeklyBusRouteService#hasDirectPath(Integer, Integer)} should return false when arrival station doesn't
     * exist.
     * 
     * @see WeeklyBusRouteService#hasDirectPath(Integer, Integer)
     * 
     * @throws Exception
     *             If anything goes wrong.
     */
    @Test
    public void hasDirectPathShouldReturnFalseWhenArrivalStationDoesNotExist() throws Exception {
        final WeeklyBusRouteService bidirectional = springLoadedBusRouteService;
        final WeeklyBusRouteService unidirectional = new WeeklyBusRouteService(EXAMPLE_FILE_PATH, false);

        // Bidirectional
        assertFalse(bidirectional.hasDirectPath(2147483647, 240));
        assertFalse(bidirectional.hasDirectPath(140, 120));
        assertFalse(bidirectional.hasDirectPath(16, 1551));
        assertFalse(bidirectional.hasDirectPath(-121, 15));
        assertFalse(bidirectional.hasDirectPath(5, 84));

        // Unidirectional
        assertFalse(unidirectional.hasDirectPath(2147483647, 240));
        assertFalse(unidirectional.hasDirectPath(140, 120));
        assertFalse(unidirectional.hasDirectPath(16, 1551));
        assertFalse(unidirectional.hasDirectPath(-121, 15));
        assertFalse(unidirectional.hasDirectPath(5, 84));
    }

    /**
     * {@link WeeklyBusRouteService#hasDirectPath(Integer, Integer)} should return false when departure station doesn't
     * exist.
     * 
     * @see WeeklyBusRouteService#hasDirectPath(Integer, Integer)
     * 
     * @throws Exception
     *             If anything goes wrong.
     */
    @Test
    public void hasDirectPathShouldReturnFalseWhenDepartureStationDoesNotExist() throws Exception {
        final WeeklyBusRouteService bidirectional = springLoadedBusRouteService;
        final WeeklyBusRouteService unidirectional = new WeeklyBusRouteService(EXAMPLE_FILE_PATH, false);

        // Bidirectional
        assertFalse(bidirectional.hasDirectPath(240, 2147483647));
        assertFalse(bidirectional.hasDirectPath(120, 140));
        assertFalse(bidirectional.hasDirectPath(1551, 16));
        assertFalse(bidirectional.hasDirectPath(15, -121));
        assertFalse(bidirectional.hasDirectPath(84, 5));

        // Unidirectional
        assertFalse(unidirectional.hasDirectPath(240, 2147483647));
        assertFalse(unidirectional.hasDirectPath(120, 140));
        assertFalse(unidirectional.hasDirectPath(1551, 16));
        assertFalse(unidirectional.hasDirectPath(15, -121));
        assertFalse(unidirectional.hasDirectPath(84, 5));
    }

    /**
     * {@link WeeklyBusRouteService#hasDirectPath(Integer, Integer)} should return false when both stations don't exist
     * <i>even if they are equal</i>.
     * 
     * @see WeeklyBusRouteService#hasDirectPath(Integer, Integer)
     * 
     * @throws Exception
     *             If anything goes wrong.
     */
    @Test
    public void hasDirectPathShouldReturnFalseWhenBothStationsDoNotExist() throws Exception {
        final WeeklyBusRouteService bidirectional = springLoadedBusRouteService;
        final WeeklyBusRouteService unidirectional = new WeeklyBusRouteService(EXAMPLE_FILE_PATH, false);

        // Bidirectional
        assertFalse(bidirectional.hasDirectPath(240, 240));
        assertFalse(bidirectional.hasDirectPath(120, 120));
        assertFalse(bidirectional.hasDirectPath(1551, 109123));
        assertFalse(bidirectional.hasDirectPath(15, -1217));
        assertFalse(bidirectional.hasDirectPath(84, 51));

        // Unidirectional
        assertFalse(unidirectional.hasDirectPath(240, 240));
        assertFalse(unidirectional.hasDirectPath(120, 120));
        assertFalse(unidirectional.hasDirectPath(1551, 109123));
        assertFalse(unidirectional.hasDirectPath(15, -1217));
        assertFalse(unidirectional.hasDirectPath(84, 51));
    }
}
