package com.goeuro.devtest.busroute.api;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goeuro.devtest.busroute.services.WeeklyBusRouteService;
import com.goeuro.devtest.busroute.viewdata.HasDirectRouteViewData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Provides the REST end-point implementation for the Weekly Bus Routes service.
 * 
 * @author aelsayed
 *
 */
@RestController

@Api(value = "Weekly Bus Route API", description = "REST API for querying the Weekly bus routes data.",
        tags = { "Weekly Bus Route API" })
public class WeeklyBusRouteRestController {
    private static final String className = WeeklyBusRouteRestController.class.getName();
    private static final Logger logger = Logger.getLogger(className);

    @Autowired
    private WeeklyBusRouteService weeklyBusRouteService;

    /**
     * Checks whether a direct path exists between the given departure and arrival stations.
     * 
     * @param departureStation
     *            Departure station Id.
     * @param arrivalStation
     *            Arrival Station Id.
     * @return True if there is a direct route between the two stations.
     */
    @GetMapping("direct")
    @ApiOperation(value = "Check if a direct route exist between departure and arrival stations.",
            response = HasDirectRouteViewData.class, nickname = "Have Direct Route",
            produces = MediaType.APPLICATION_JSON_VALUE)

    public HasDirectRouteViewData hasDirectRoute(
            @ApiParam(name = "dep_sid",
                    value = "Departure Station ID") @RequestParam("dep_sid") final int departureStation,
            @ApiParam(name = "arr_sid",
                    value = "Arrival Station ID") @RequestParam("arr_sid") final int arrivalStation) {
        logger.entering(className, "hasDirectRoute", departureStation + ", " + arrivalStation);

        logger.info("/direct invoked with dep_sid=" + departureStation + " and arr_sid=" + arrivalStation);
        
        // weeklyBusRouteService.hasDirectPath does not throw any exceptions because all parsing logic is performed
        // during class loading.
        final boolean hasDirectRoute = weeklyBusRouteService.hasDirectPath(departureStation, arrivalStation);
        final HasDirectRouteViewData result =
                new HasDirectRouteViewData(departureStation, arrivalStation, hasDirectRoute);
        logger.exiting(className, "hasDirectRoute", hasDirectRoute);
        return result;
    }
}
