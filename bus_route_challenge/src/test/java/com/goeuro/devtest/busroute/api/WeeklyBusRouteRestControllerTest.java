package com.goeuro.devtest.busroute.api;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.goeuro.devtest.busroute.services.WeeklyBusRouteService;

/**
 * Test class for {@link WeeklyBusRouteRestController}.
 * 
 * @author aelsayed
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(WeeklyBusRouteRestController.class)
public class WeeklyBusRouteRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private WeeklyBusRouteService weeklyBusRouteService;

    /**
     * Test method for {@link WeeklyBusRouteRestController#hasDirectRoute(int, int)}.<br>
     * 
     * @throws Exception
     *             if anything goes wrong.
     */
    @Test
    @SuppressWarnings("PMD.SignatureDeclareThrowsException") // This is a test case. It is OK to fail by throwing
                                                             // Exception.
    public void testHasDirectRoute() throws Exception {
        // Setup the service layer mock
        final int mockDepartureStation = 10;
        final int mockArrivalStation = 20;
        given(weeklyBusRouteService.hasDirectPath(mockDepartureStation, mockArrivalStation)).willReturn(true);

        // Invoke the REST service
        mvc.perform(get("/direct?dep_sid=10&arr_sid=20").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("dep_sid", is(mockDepartureStation)))
                .andExpect(jsonPath("arr_sid", is(mockArrivalStation)))
                .andExpect(jsonPath("direct_bus_route", is(true)));
    }
}
