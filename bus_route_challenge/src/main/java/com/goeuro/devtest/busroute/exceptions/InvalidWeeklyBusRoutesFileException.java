package com.goeuro.devtest.busroute.exceptions;

/**
 * Thrown to indicate that a given file does not match the specifications of the Weekly Bus Routes file.<br>
 * Valid files must:
 * <ul>
 * <li>Start with a single integer value [n] (the number of routes)</li>
 * <li>Followed by n lines of space-separated integers (route id followed by station ids)</li>
 * </ul>
 * 
 * @author aelsayed
 *
 */
public class InvalidWeeklyBusRoutesFileException extends Exception {

    private static final long serialVersionUID = 1L;
    private static final String DEFAULT_MESSAGE = "Invalid Weekly Bus Route File";

    /**
     * Initializes the exception with the default message.<br>
     * {@value #DEFAULT_MESSAGE}
     */
    public InvalidWeeklyBusRoutesFileException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            The exception message.
     * @param cause
     *            The exception that was wrapped by this exception.
     */
    public InvalidWeeklyBusRoutesFileException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            The exception message.
     */
    public InvalidWeeklyBusRoutesFileException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param cause
     *            The exception that was wrapped by this exception.
     */
    public InvalidWeeklyBusRoutesFileException(final Throwable cause) {
        super(cause);
    }

}
