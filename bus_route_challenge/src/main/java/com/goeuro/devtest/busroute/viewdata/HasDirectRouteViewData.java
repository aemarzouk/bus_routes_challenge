package com.goeuro.devtest.busroute.viewdata;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * POJO class representing the data to be returned by the <code>api/direct</code> REST service.
 * 
 * @author aelsayed
 *
 */
@ApiModel(value = "Has Direct Route Response", description = "Lists the departure station, arrival station and indicates whether there exists a direct path between them.")
@JsonPropertyOrder({ "dep_sid", "arr_sid", "direct_bus_route" }) // Maintaining the serialization order.
public class HasDirectRouteViewData {
    @ApiModelProperty(name = "dep_sid", required = true, value = "Departure Station ID")
    @NotNull
    private int departureStationID;

    @ApiModelProperty(name = "arr_sid", required = true, value = "Arrival Station ID")
    @NotNull
    private int arrivalStationID;

    @ApiModelProperty(name = "direct_bus_route", required = true, value = "Direct bus route exists between departure station and arrival station?")
    @NotNull
    private boolean hasDirectRout;

    /**
     * Constructor.
     * 
     * @param departureStationID
     *            Departure station ID.
     * @param arrivalStationID
     *            Arrival station ID.
     * @param hasDirectRout
     *            Indicates whether there is a direct bus route between the two stations.
     */
    public HasDirectRouteViewData(final int departureStationID, final int arrivalStationID,
            final boolean hasDirectRout) {
        super();
        this.departureStationID = departureStationID;
        this.arrivalStationID = arrivalStationID;
        this.hasDirectRout = hasDirectRout;
    }

    /**
     * Getter.
     * 
     * @return the departureStationID
     */
    @JsonGetter("dep_sid")
    public int getDepartureStationID() {
        return departureStationID;
    }

    /**
     * Getter.
     * 
     * @return the arrivalStationID
     */
    @JsonGetter("arr_sid")
    public int getArrivalStationID() {
        return arrivalStationID;
    }

    /**
     * Getter.
     * 
     * @return the hasDirectRout
     */
    @JsonGetter("direct_bus_route")
    public boolean isHasDirectRout() {
        return hasDirectRout;
    }

}
