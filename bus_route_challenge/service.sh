#!/usr/bin/env bash

# This script will run under bus_route_challenge folder irrespective of where it was invoked from
cd $(dirname $0)

dev_build() {
  # Build the app
  mvn clean package
}

dev_site() {
  # Generate the site
  mvn site
}

dev_run() {
  # Validate that a command line argument was passed (file path)
  if [ $# -eq 0 ]
  then
    echo "Cannot run the application without the input file path command line argument."
    exit 1
  fi
  # Run the app in the foreground
  java -jar target/bus_route_challenge-1.0.jar --filePath=$*
}

dev_smoke() {
  if _run_smoke; then
    echo "Tests Passed"
    exit 0
  else
    echo "Tests Failed"
    exit 1
  fi
}

_run_smoke() {
  baseUrl="http://localhost:8088"
  echo "Running smoke tests on $baseUrl..." && \
    (curl -fsS "$baseUrl/api/direct?dep_sid=3&arr_sid=4" | grep -E 'true|false') && \
    (curl -fsS "$baseUrl/api/direct?dep_sid=0&arr_sid=1" | grep -E 'true|false')
}

docker_build() {
  if [ $# -eq 0 ]
  then
    echo "Please set the input file path command line argument."
    exit 1
  fi
  dev_build
  docker build . -t goeuro:devtest --build-arg inputFile=$*
}

docker_run() {
  docker run --rm -it -p 8088:8088 goeuro:devtest
}

docker_smoke() {
  containerId=$(docker run -d goeuro:devtest)
  echo "Waiting 30 seconds for service to start..."
  echo "Application startup may take longer with large datasets as data indexing is performed during initialization."
  sleep 30
  docker exec $containerId /service.sh dev_smoke
  retval=$?
  docker rm -f $containerId
  exit $retval
}

usage() {
  cat <<EOF
Usage:
  $0 <command> <args>
Local machine commands:
  dev_build           : builds and packages the application
  dev_run <file>      : starts your app in the foreground. <file> is mandatory
  dev_smoke           : runs a smoke test on localhost:8088
  dev_site            : Generates the project's site (project info, Java Doc, Junit and Code Coverage reports)
Docker commands:
  docker_build <file> : packages the app into a docker image and copies the <file> into it
  docker_run          : runs your app using a docker image
  docker_smoke        : runs same smoke tests inside a docker container
EOF
}

action=$1
action=${action:-"usage"}
action=${action/help/usage}
shift
if type -t $action >/dev/null; then
  echo "Invoking: $action"
  $action $*
else
  echo "Unknown action: $action"
  usage
  exit 1
fi