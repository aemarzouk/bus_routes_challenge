/**
 * This package holds all the custom exceptions thrown by the application.
 * 
 * @author aelsayed
 *
 */
package com.goeuro.devtest.busroute.exceptions;
