/**
 * This package holds the POJO data transfer classes that are meant to be exposed publicly.
 * 
 * @author aelsayed
 *
 */
package com.goeuro.devtest.busroute.viewdata;
