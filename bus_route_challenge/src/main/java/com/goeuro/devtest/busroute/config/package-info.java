/**
 * This package holds the different classes used to configure various aspects of the application.
 * 
 * @author aelsayed
 *
 */
package com.goeuro.devtest.busroute.config;
